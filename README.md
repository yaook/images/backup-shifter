<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# backup-shifter

Simple docker container to move backup data off a system and optionally retain
a local stash.

## Concept

`backup-shifter` uses a directory tree rooted at `$BACKUP_SHIFTER_BASEDIR`. In
that tree, the following directories are used:

- `new`: New, completed backups must be placed here atomically.
- `scratchpad`: Internal scratchpad for `backup-shifter`.
- `warm`: Local storage for completed backups after they have been processed by
  `backup-shifter`.

To place a backup for processing by `backup-shifter` it MUST be placed using a
`rename(2)` (or `mv(1)` on the same filesystem) invocation. Otherwise, it will
not be processed. Ideally, the upstream backup software places their backups
into a directory called `incoming` next to `new` and, as a final step, invokes
`mv` to place the backup in `new`.

The backup will then be processed by `backup-shifter`; the first step is to
create a `tar` file (if the backup is a directory). Then the file (either
`tar` or the original) will be handed to the shifter plugins in the order they
are configured in `$BACKUP_SHIFTER_SHIFTERS`. If all shifters succeed, the
backup is moved to `warm` and the retention policy for local backup storage is
invoked.

When a shifter fails, the process is aborted and not retried until the next
`rename` (`moved_to`) event arrives.

## Configuration

The backup-shifter is configured using environment variables.

- `YAOOK_BACKUP_SHIFTER_BASEDIR`: Must be the path to the directory tree used by
  `backup-shifter`.
- `YAOOK_BACKUP_SHIFTER_SHIFTERS`: Space-separated list of shifter plugins
  (executables in `/app/shifters`) to invoke on each backup.
- `YAOOK_BACKUP_SHIFTER_WARM_KEEP_MAX` (default: 3): How many backups to keep locally
  at most.
- `YAOOK_BACKUP_SHIFTER_WARM_KEEP_FOR` (default: unset): If set, must be an integer.
  Backups which are older than the given number of days will be deleted (before
  enforcing `BACKUP_SHIFTER_WARM_KEEP_MAX`).
- `YAOOK_BACKUP_SHIFTER_METRICS_PORT` (default: 9100): Port that will be used by
  the Prometheus client to expose metrics.

## Extension

To add your own scripts for moving backups off the local storage, mount the
executable files to /app/shifters/. You can then invoke them by adding them
to the `YAOOK_BACKUP_SHIFTER_SHIFTERS` environment variable.

## Environment Variables for shifters

The following are the environment variables that are used for the respective shifters.

### s3_upload

Note that this shifter does not delete any backup on the s3.
Please use a lifecycle policy on your s3 service for this.

| Environment Variables                    | Usage |
| ---------------------------------------- | ----- |
| YAOOK_BACKUP_SHIFTER_S3_URL              | The URL for the S3 Storage service |
| YAOOK_BACKUP_SHIFTER_S3_BUCKET           | The Name of the Bucket on the S3 Service |
| YAOOK_BACKUP_SHIFTER_S3_FILE_PREFIX      | (optional) A prefix being prepended to the filename of the uploaded file |
| YAOOK_BACKUP_SHIFTER_S3_ACCESS_KEY       | The S3 Access Key |
| YAOOK_BACKUP_SHIFTER_S3_SECRET_KEY       | The S3 Secret Key |
| YAOOK_BACKUP_SHIFTER_S3_ADDRESSING_STYLE | (optional) The S3 Addressing Style. Defaults to "virtual" for subdomains. Can be set to "path" for using path-style services |
| YAOOK_BACKUP_SHIFTER_S3_CACERT           | (optional) A path to the CA Certificate to use. If unset disables certificate validation |

## License

[Apache 2](LICENSE.txt)