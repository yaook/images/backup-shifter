#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from backup_shifter.backup_shifter import BackupShifter
from datetime import datetime, timedelta
import ddt
from inotify.constants import MASK_LOOKUP, IN_MOVED_TO, IN_ISDIR
import math
import os
from pathlib import Path
import subprocess
import tarfile
import tempfile
import unittest
import unittest.mock


@ddt.ddt
class TestBackupShifter(unittest.TestCase):
    """Tests the class BackupShifter.

    We test the public methods, __init__ and start as well as the private
    methods _stale_processing, _remove_old_warm_files, _on_new and _on_warm.
    """
    def setUp(self):
        super().setUp()
        self.work_dir = tempfile.TemporaryDirectory()
        self.shifters_dir = tempfile.TemporaryDirectory()
        self.shifters_dir_path = Path(self.shifters_dir.name)

        self.shifter_a = self.shifters_dir_path / "shifter_a"
        with open(self.shifter_a, "w") as f:
            f.write("#!/bin/bash")
        self.shifter_a.chmod(0o755)

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": self.work_dir.name,
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
        }):

            self.shifter = BackupShifter()

        self.new_dir = Path(self.work_dir.name, "new")
        self.warm_dir = Path(self.work_dir.name, "warm")
        self.scratch_dir = Path(self.work_dir.name, "scratch")

        self.new_file_event = \
            (None, [MASK_LOOKUP[IN_MOVED_TO]], self.new_dir, "new_dummy_file")
        self.warm_file_event = \
            (None, [MASK_LOOKUP[IN_MOVED_TO]], self.warm_dir,
             "warm_dummy_file")
        self.new_dir_event = (
            None,
            [MASK_LOOKUP[IN_MOVED_TO], MASK_LOOKUP[IN_ISDIR]],
            self.new_dir,
            "new_test_dir",
        )

    def test___init__(self):
        expected_work_dir = Path(self.work_dir.name)
        expected_shifters_dir = Path(self.shifters_dir.name)
        expected_num_days = 5
        expected_num_backups = 4
        expected_compress = False
        expected_interval = 10

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": expected_work_dir.as_posix(),
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR":
                expected_shifters_dir.as_posix(),
            "YAOOK_BACKUP_SHIFTER_NUM_DAYS": str(expected_num_days),
            "YAOOK_BACKUP_SHIFTER_NUM_BACKUPS": str(expected_num_backups),
            "YAOOK_BACKUP_SHIFTER_COMPRESS": str(expected_compress),
            "YAOOK_BACKUP_SHIFTER_INTERVAL": str(expected_interval),
        }):
            shifter = BackupShifter()

        self.assertEqual(expected_work_dir, shifter.work_dir)
        self.assertEqual(expected_work_dir / "new", shifter.new_dir)
        self.assertEqual(expected_work_dir / "warm", shifter.warm_dir)
        self.assertEqual(expected_work_dir / "scratch", shifter.scratch_dir)
        self.assertEqual(expected_shifters_dir, shifter.shifters_dir)
        self.assertEqual(1, len(shifter.shifters))
        self.assertIn(self.shifter_a, shifter.shifters)
        self.assertEqual(expected_num_days, shifter.num_days)
        self.assertEqual(expected_num_backups, shifter.num_backups)
        self.assertEqual(expected_interval, shifter._processing_interval.days)
        self.assertEqual("w", shifter._archive_mode)
        self.assertEqual("tar", shifter._archive_filename_ext)

    def test___init___defaults(self):
        expected_work_dir = Path(self.work_dir.name)
        expected_shifters_dir = Path(self.shifters_dir.name)

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": expected_work_dir.as_posix(),
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR":
                expected_shifters_dir.as_posix(),
        }):
            shifter = BackupShifter()

        self.assertEqual(expected_work_dir, shifter.work_dir)
        self.assertEqual(expected_work_dir / "new", shifter.new_dir)
        self.assertEqual(expected_work_dir / "warm", shifter.warm_dir)
        self.assertEqual(expected_work_dir / "scratch", shifter.scratch_dir)
        self.assertEqual(expected_shifters_dir, shifter.shifters_dir)
        self.assertEqual(1, len(shifter.shifters))
        self.assertIn(self.shifter_a, shifter.shifters)
        self.assertEqual(-1, shifter.num_days)
        self.assertEqual(3, shifter.num_backups)
        self.assertEqual(1, shifter._processing_interval.days)
        self.assertEqual("w:gz", shifter._archive_mode)
        self.assertEqual("tgz", shifter._archive_filename_ext)

    def test___init___resolves_shifters(self):
        expected_work_dir = Path(self.work_dir.name)
        expected_shifters_dir = Path(self.shifters_dir.name)

        shifter_a = expected_shifters_dir / "shifter_a"
        with open(shifter_a, "w"):
            pass
        shifter_a.chmod(0o755)

        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": expected_work_dir.as_posix(),
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR":
                expected_shifters_dir.as_posix(),
        }):
            shifter = BackupShifter()

        self.assertEqual(1, len(shifter.shifters))
        self.assertIn(shifter_a, shifter.shifters)

    def test___init___raises_invalid_work_dir(self):
        invalid_work_dir = "invalid/work/dir"
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": invalid_work_dir,
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
        }):
            with self.assertRaisesRegex(EnvironmentError, invalid_work_dir):
                BackupShifter()

    def test___init___raises_invalid_shifters_dir(self):
        invalid_shifters_dir = "invalid/shifters/dir"
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": invalid_shifters_dir,
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.work_dir.name,
        }):
            with self.assertRaisesRegex(EnvironmentError,
                                        invalid_shifters_dir):
                BackupShifter()

    def test___init___raises_missing_work_dir(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
        }):
            with self.assertRaisesRegex(EnvironmentError,
                                        "YAOOK_BACKUP_SHIFTER_WORK_DIR"):
                BackupShifter()

    def test___init___raises_missing_shifters_dir(self):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": self.work_dir.name,
        }):
            with self.assertRaisesRegex(EnvironmentError,
                                        "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR"):
                BackupShifter()

    @ddt.data(-2, -1, 0, 1)
    def test___init___sets_processing_interval(self, interval):
        with unittest.mock.patch.dict(os.environ, {
            "YAOOK_BACKUP_SHIFTER_WORK_DIR": self.work_dir.name,
            "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
            "YAOOK_BACKUP_SHIFTER_INTERVAL": str(interval),
        }):
            shifter = BackupShifter()

        self.assertEqual(
            timedelta(interval) if interval > 0 else None,
            shifter._processing_interval
        )

    def test__resolve_shifters_without_filter(self):
        shifter_b = self.shifters_dir_path / "shifter_b"
        with open(shifter_b, "w"):
            pass
        shifter_b.chmod(0o755)

        shifters = self.shifter._resolve_shifters(None)

        self.assertEqual(2, len(shifters))
        self.assertIn(self.shifter_a, shifters)
        self.assertIn(shifter_b, shifters)

    def test__resolve_shifters_with_filter(self):
        shifter_b = self.shifters_dir_path / "shifter_b"
        with open(shifter_b, "w"):
            pass
        shifter_b.chmod(0o755)
        shifter_c = self.shifters_dir_path / "shifter_c"
        with open(shifter_c, "w"):
            pass
        shifter_c.chmod(0o755)

        shifters = self.shifter._resolve_shifters("shifter_a,shifter_c")

        self.assertEqual(2, len(shifters))
        self.assertIn(self.shifter_a, shifters)
        self.assertIn(shifter_c, shifters)

    def test__resolve_shifters_with_filter_ordered(self):
        shifter_b = self.shifters_dir_path / "shifter_b"
        with open(shifter_b, "w"):
            pass
        shifter_b.chmod(0o755)

        shifters = self.shifter._resolve_shifters("shifter_b,shifter_a")

        self.assertEqual(2, len(shifters))
        self.assertEqual(shifter_b, shifters[0])
        self.assertEqual(self.shifter_a, shifters[1])

    def test__resolve_shifters_fails_non_executable_shifter(self):
        shifter_b = self.shifters_dir_path / "shifter_b"
        with open(shifter_b, "w"):
            pass

        self.assertRaises(ValueError, self.shifter._resolve_shifters, None)

    def test__resolve_shifters_fails_non_file_shifter(self):
        shifter_b = self.shifters_dir_path / "shifter_b"
        shifter_b.mkdir()

        self.assertRaises(ValueError, self.shifter._resolve_shifters, None)

    @unittest.mock.patch("inotify.adapters.Inotify")
    def test_start_sets_up_inotify(self, mock_inotify):
        self.shifter.start(loop_forever=False)

        mock_inotify.return_value.add_watch.assert_has_calls(
            [
                unittest.mock.call(self.new_dir.as_posix(), mask=IN_MOVED_TO),
                unittest.mock.call(self.warm_dir.as_posix(), mask=IN_MOVED_TO),
            ],
            any_order=True,
        )
        mock_inotify.return_value.event_gen\
            .assert_called_once_with(yield_nones=True)

    @ddt.data(True, False)  # whether _on_new() succeeds
    def test_start_does_initial_processing(self, on_new_success):
        # Patching inotify without making event_gen() return any events
        # makes sure the event loop is never entered.
        self.patch("inotify.adapters.Inotify")

        # The local variable warm_file prevents the temp file from being
        # deleted
        # If the file does not exist _on_warm() will not be called
        warm_file = tempfile.NamedTemporaryFile(  # noqa: F841
            dir=self.warm_dir)

        # setting up a few new files with which _on_new() should be called.
        new_file = tempfile.NamedTemporaryFile(dir=self.new_dir)
        new_dir = tempfile.TemporaryDirectory(dir=self.new_dir)

        self.shifter._remove_old_warm_files = unittest.mock.MagicMock()
        self.shifter._on_new = unittest.mock.MagicMock()
        self.shifter._on_new.return_value = on_new_success
        self.shifter._on_warm = unittest.mock.MagicMock()

        # call method under test
        self.shifter.start(loop_forever=False)

        # assert the mocked private methods are called correctly
        self.shifter._remove_old_warm_files.assert_called_once()
        self.shifter._on_warm.assert_called_once_with()
        expected_new_path = Path(new_file.name)
        expected_new_dir_path = Path(new_dir.name)
        self.shifter._on_new.assert_has_calls(
            [
                unittest.mock.call(expected_new_path),
                unittest.mock.call(expected_new_dir_path),
            ],
            any_order=True
        )
        self.assertSetEqual(
            self.shifter._stale_new_files,
            set()
            if on_new_success
            else {expected_new_path, expected_new_dir_path}
        )

    def test_start_processes_stale_periodically(self):
        processing_interval = 2  # days
        num_events = 5 * processing_interval

        num_clock_ticks = 0

        def stub_now():
            nonlocal num_clock_ticks
            num_clock_ticks += 24 * 60 * 60  # number of seconds in a day
            return float(num_clock_ticks)

        with unittest.mock.patch('backup_shifter.backup_shifter.time'
                                 ) as mock_time:
            mock_time.monotonic.side_effect = stub_now

            mock_event_gen = self.patch("inotify.adapters.Inotify.event_gen")
            mock_event_gen.return_value = [None] * num_events

            with unittest.mock.patch.dict(os.environ, {
                "YAOOK_BACKUP_SHIFTER_WORK_DIR": self.work_dir.name,
                "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
                "YAOOK_BACKUP_SHIFTER_INTERVAL": str(processing_interval),
            }):
                shifter = BackupShifter()

            mock_stale_processing = \
                self.patch_object(shifter, "_stale_processing")

            shifter.start(loop_forever=False)

            # We modified the current time by stubbing time.monotonic() in
            # stub_now() so that we only get one event per day. This way
            # expected number of calls to _stale_processing =
            # num_events/processing_interval in days
            # plus 1 for the initial call before the event loop.
            expected_num_calls = \
                math.floor(num_events / processing_interval) + 1
            self.assertEqual(
                mock_stale_processing.call_count,
                expected_num_calls,
            )
            mock_stale_processing.assert_has_calls(
                [unittest.mock.call()] * expected_num_calls
            )

    @ddt.data(0, -1)
    def test_start_does_not_process_stale_periodically(self, interval):
        num_clock_ticks = 0

        def stub_now():
            nonlocal num_clock_ticks
            num_clock_ticks += 24 * 60 * 60  # number of seconds in a day
            return float(num_clock_ticks)

        with unittest.mock.patch('backup_shifter.backup_shifter.time'
                                 ) as mock_time:
            mock_time.monotonic.side_effect = stub_now

            mock_event_gen = self.patch("inotify.adapters.Inotify.event_gen")
            mock_event_gen.return_value = [None] * 10

            with unittest.mock.patch.dict(os.environ, {
                "YAOOK_BACKUP_SHIFTER_WORK_DIR": self.work_dir.name,
                "YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR": self.shifters_dir.name,
                "YAOOK_BACKUP_SHIFTER_INTERVAL": str(interval),
            }):
                shifter = BackupShifter()

            mock_stale_processing = \
                self.patch_object(shifter, "_stale_processing")

            shifter.start(loop_forever=False)

            # The expected number of calls to _stale_processing = 0 (since we
            # disabled it) plus 1 for the initial call before the event loop.
            mock_stale_processing.assert_called_once_with()

    @ddt.data(True, False)
    def test_start_interprets_new_file_event(self, on_new_success):
        mock_event_gen = self.patch("inotify.adapters.Inotify.event_gen")
        expected_event = self.new_file_event
        mock_event_gen.return_value = [expected_event]

        mock_on_new = self.patch_object(self.shifter, "_on_new")
        mock_on_new.return_value = on_new_success
        mock_on_warm = self.patch_object(self.shifter, "_on_warm")

        self.shifter.start(loop_forever=False)

        expected_file = expected_event[2] / expected_event[3]
        mock_on_new.assert_called_once_with(expected_file)
        mock_on_warm.assert_not_called()
        self.assertSetEqual(
            self.shifter._stale_new_files,
            set() if on_new_success else {expected_file}
        )

    def test_start_interprets_warm_file_event(self):
        mock_event_gen = self.patch("inotify.adapters.Inotify.event_gen")
        mock_event_gen.return_value = [self.warm_file_event]

        mock_on_new = self.patch_object(self.shifter, "_on_new")
        mock_on_warm = self.patch_object(self.shifter, "_on_warm")

        self.shifter.start(loop_forever=False)

        mock_on_warm.assert_called_once_with()
        mock_on_new.assert_not_called()

    @ddt.data(False, True)
    def test_start_interprets_new_dir_event(self, on_new_success):
        mock_event_gen = self.patch("inotify.adapters.Inotify.event_gen")
        mock_event_gen.return_value = [self.new_dir_event]

        mock_on_new = self.patch_object(self.shifter, "_on_new")
        mock_on_new.return_value = on_new_success
        mock_on_warm = self.patch_object(self.shifter, "_on_warm")

        self.shifter.start(loop_forever=False)

        expected_dir = self.new_dir_event[2] / self.new_dir_event[3]
        mock_on_new.assert_called_once_with(expected_dir)
        mock_on_warm.assert_not_called()
        self.assertSetEqual(
            self.shifter._stale_new_files,
            set() if on_new_success else {expected_dir}
        )

    @ddt.data(tempfile.TemporaryDirectory, tempfile.NamedTemporaryFile)
    def test_start_processes_stale_new_file_initially(self, file_or_dir_class):
        # Patching inotify without making event_gen() return any events
        # makes sure the event loop is never entered.
        self.patch("inotify.adapters.Inotify")

        mock_remove_old_files = \
            self.patch_object(self.shifter, "_remove_old_warm_files")
        mock_on_new = self.patch_object(self.shifter, "_on_new")

        file_or_dir = file_or_dir_class(dir=self.new_dir)
        self.assertTrue(os.path.exists(file_or_dir.name))

        self.shifter.start(loop_forever=False)

        mock_on_new.assert_called_once_with(Path(file_or_dir.name))
        mock_remove_old_files.assert_called_once()

    @ddt.data(True, False)
    def test__stale_processing(self, on_new_success):
        mock_remove_old_warm_files = \
            self.patch_object(self.shifter, "_remove_old_warm_files")
        mock_on_new = self.patch_object(self.shifter, "_on_new")
        mock_on_new.return_value = on_new_success

        stale_file = Path(tempfile.NamedTemporaryFile(dir=self.new_dir).name)
        self.shifter._stale_new_files = {stale_file}

        self.shifter._stale_processing()

        mock_on_new.assert_called_once_with(stale_file)
        mock_remove_old_warm_files.assert_called_once()
        self.assertSetEqual(
            self.shifter._stale_new_files,
            set() if on_new_success else {stale_file}
        )

    def test__remove_old_warm_files(self):
        with unittest.mock.patch('backup_shifter.backup_shifter.Path.stat'
                                 ) as mock_stat:
            # a long time ago
            stat_result = os.stat_result((0, 1, 2, 3, 4, 5, 6, 7, 8, 9))
            mock_stat.return_value = stat_result

            old_warm_file = tempfile.NamedTemporaryFile(dir=self.warm_dir)
            self.assertTrue(os.path.exists(old_warm_file.name))

            self.shifter.num_days = 1

            self.shifter._remove_old_warm_files()

            self.assertFalse(os.path.exists(old_warm_file.name))

    # the value of the num_backups attribute of the shifter
    @ddt.data(-2, -1, 0, 5, 7)
    @unittest.mock.patch("pathlib.Path.iterdir")
    def test__on_warm_deletes_backups(self, num_backups, mock_path_iterdir):
        total_num_warm_files = 7
        # the test must not use too high of a value for num_backups
        self.assertFalse(total_num_warm_files < num_backups)

        if num_backups > 0:
            expected_num_kept = num_backups
        else:
            expected_num_kept = total_num_warm_files
        expected_num_deleted = total_num_warm_files - expected_num_kept

        expected_deleted = [
            unittest.mock.MagicMock()
            for _ in range(expected_num_deleted)
        ]
        expected_kept = [
            unittest.mock.MagicMock()
            for _ in range(expected_num_kept)
        ]

        # We use the integer suffix to mock the mtime.
        # A lower suffix means the mtime is lower, i.e. the file older.
        # Therefore the files to be kept should have higher suffixes, than the
        # ones to be deleted.
        for i, mock_obj in enumerate(expected_deleted):
            mtime_suffix = i
            mock_obj.as_posix.return_value = f"expected_deleted_{mtime_suffix}"
            mock_obj.stat.return_value.st_mtime_ns = mtime_suffix
            mock_obj.stat.return_value.st_mtime = mtime_suffix
        for i, mock_obj in enumerate(expected_kept):
            mtime_suffix = expected_num_deleted + i
            mock_obj.as_posix.return_value = f"expected_kept_{mtime_suffix}"
            mock_obj.stat.return_value.st_mtime_ns = mtime_suffix
            mock_obj.stat.return_value.st_mtime = mtime_suffix

        mock_path_iterdir.return_value = expected_deleted + expected_kept

        self.shifter.num_backups = num_backups

        self.shifter._on_warm()

        [f.unlink.assert_not_called() for f in expected_kept]
        [f.unlink.assert_called_once_with() for f in expected_deleted]

    def test__on_new_dir(self):
        with unittest.mock.patch('backup_shifter.backup_shifter.datetime'
                                 ) as mock_datetime:
            now_str = "20210510_19:38:10.999999"
            now = datetime(
                2021, 5, 10, hour=19, minute=38, second=10, microsecond=999999
            )
            mock_datetime.now.return_value = now
            mock_datetime.side_effect = \
                lambda *args, **kw: datetime(*args, **kw)

            new_new_dir = tempfile.TemporaryDirectory(dir=self.new_dir)
            new_path = Path(new_new_dir.name)

            observed_success = self.shifter._on_new(new_path)

            self.assertTrue(observed_success)

            pid = os.getpid()
            archive_filename = \
                f"automatically_archived-{pid}-{now_str}-{new_path.name}.tgz"
            expected_archive = self.new_dir / archive_filename
            observed_new_dir_content = set(self.new_dir.iterdir())
            self.assertSetEqual({expected_archive}, observed_new_dir_content)
            self.assertTrue(tarfile.is_tarfile(expected_archive.as_posix()))

            observed_scratch_dir_content = set(self.scratch_dir.iterdir())
            self.assertSetEqual(set(), observed_scratch_dir_content)

            observed_warm_dir_content = set(self.warm_dir.iterdir())
            self.assertSetEqual(set(), observed_warm_dir_content)

    @unittest.mock.patch("backup_shifter.backup_shifter.Path.rename")
    @unittest.mock.patch("backup_shifter.backup_shifter.datetime")
    def test__on_new_file_uses_mv(self, mock_datetime, mock_rename):
        now = datetime(
            2021, 5, 10, hour=19, minute=38, second=10, microsecond=999999
        )
        mock_datetime.now.return_value = now
        mock_datetime.side_effect = \
            lambda *args, **kw: datetime(*args, **kw)
        now_str = "20210510_19:38:10.999999"

        temp_file = tempfile.NamedTemporaryFile(dir=self.new_dir)
        new_file = Path(temp_file.name)

        observed_success = self.shifter._on_new(new_file)

        self.assertTrue(observed_success)

        mock_rename.assert_called_once_with(
            self.warm_dir / f"{now_str}-{new_file.name}"
        )

    @unittest.mock.patch("backup_shifter.backup_shifter.Path.rename")
    @unittest.mock.patch("backup_shifter.backup_shifter.datetime")
    def test__on_new_dir_uses_mv(self, mock_datetime, mock_rename):
        now = datetime(
            2021, 5, 10, hour=19, minute=38, second=10, microsecond=999999
        )
        mock_datetime.now.return_value = now
        mock_datetime.side_effect = \
            lambda *args, **kw: datetime(*args, **kw)
        now_str = "20210510_19:38:10.999999"

        temp_dir = tempfile.TemporaryDirectory(dir=self.new_dir)
        new_file = Path(temp_dir.name)

        observed_success = self.shifter._on_new(new_file)

        self.assertTrue(observed_success)

        pid = os.getpid()
        expected_filename = \
            f"automatically_archived-{pid}-{now_str}-{new_file.name}.tgz"
        mock_rename.assert_called_once_with(self.new_dir / expected_filename)

    @unittest.mock.patch("tarfile.open")
    @unittest.mock.patch("backup_shifter.backup_shifter.Path.unlink")
    def test__on_new_dir_removes_scratch_file_on_failure(
            self, mock_unlink, mock_tar_open):
        mock_tar_open.side_effect = tarfile.TarError

        observed_success = self.shifter._on_new(unittest.mock.MagicMock())

        self.assertEqual(observed_success, False)
        mock_unlink.assert_called_once_with()

    def test__on_new_file(self):
        with unittest.mock.patch('backup_shifter.backup_shifter.datetime'
                                 ) as mock_datetime:
            # mock the date to be able to predict the new warm filename
            now_str = "20210510_19:38:10.999999"
            now = datetime(
                2021, 5, 10, hour=19, minute=38, second=10, microsecond=999999
            )
            mock_datetime.now.return_value = now
            mock_datetime.side_effect = \
                lambda *args, **kw: datetime(*args, **kw)

            # setup the new file to receive
            new_new_file = tempfile.NamedTemporaryFile(dir=self.new_dir)
            new_path = Path(new_new_file.name)

            observed_success = self.shifter._on_new(new_path)

            self.assertTrue(observed_success)

            observed_new_dir_content = set(self.new_dir.iterdir())
            self.assertSetEqual(set(), observed_new_dir_content)

            observed_scratch_dir_content = set(self.scratch_dir.iterdir())
            self.assertSetEqual(set(), observed_scratch_dir_content)

            observed_warm_dir_content = set(self.warm_dir.iterdir())
            self.assertSetEqual(
                {self.warm_dir / f"{now_str}-{new_path.name}"},
                observed_warm_dir_content
            )

            self.assertSetEqual(set(), self.shifter._stale_new_files)

    @unittest.mock.patch("subprocess.run")
    def test__on_new_runs_shifter(self, mock_run):
        new_file = tempfile.NamedTemporaryFile(dir=self.new_dir)

        mock_run.return_value = unittest.mock.MagicMock()

        observed_success = self.shifter._on_new(Path(new_file.name))

        self.assertTrue(observed_success)
        mock_run.assert_has_calls(
            [
                unittest.mock.call([self.shifter_a.as_posix(), new_file.name]),
            ],
            any_order=True,
        )
        self.assertEqual(mock_run.return_value.check_returncode.call_count, 1)

    @unittest.mock.patch("subprocess.run")
    def test__on_new_reports_failing_shifter(self, mock_run):
        new_file = tempfile.NamedTemporaryFile(dir=self.new_dir)
        new_path = Path(new_file.name)

        def stub_called_process_error():
            raise subprocess.CalledProcessError(returncode=1, cmd="dummy")

        mock_run_result = unittest.mock.MagicMock()
        mock_run.return_value = mock_run_result
        mock_run_result.check_returncode.side_effect = \
            stub_called_process_error

        observed_success = self.shifter._on_new(new_path)

        self.assertFalse(observed_success)
        self.assertTrue(self.shifter_a.exists())  # sanity check
        mock_run.assert_called_once_with(
            [self.shifter_a.as_posix(), new_path.as_posix()]
        )
        mock_run_result.check_returncode.assert_called_once_with()

    def patch(self, to_patch):
        patcher = unittest.mock.patch(to_patch)
        mock = patcher.start()
        self.addCleanup(patcher.stop)
        return mock

    def patch_object(self, o, to_patch):
        patcher = unittest.mock.patch.object(o, to_patch)
        mock = patcher.start()
        self.addCleanup(patcher.stop)
        return mock
