#!/usr/bin/env python
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import attr as environ_attr
from datetime import datetime, timedelta, timezone
import environ
from enum import Enum
import logging.config
import os
from pathlib import Path
import shutil
import subprocess
import tarfile
import time
import typing

from inotify import adapters
from inotify.constants import IN_MOVED_TO, IN_ISDIR, MASK_LOOKUP

from prometheus_client import Gauge

LAST_RUN_DURATION = Gauge(
    'yaook_backup_shifter_last_run_duration_seconds',
    'Duration of the run on the most recent backup',
    ['shifter']
    )
LAST_RUN_SUCCESS = Gauge(
    'yaook_backup_shifter_last_run_success',
    'Success/failure (1/0) code of the run on the most recent backup',
    ['shifter']
    )
LAST_RUN_TIMESTAMP = Gauge(
    'yaook_backup_shifter_last_run_timestamp_seconds',
    'Timestamp of the last attempted run',
    ['shifter']
    )
LAST_RUN_FILE_TIMESTAMP = Gauge(
    'yaook_backup_shifter_last_run_file_timestamp_seconds',
    'Timestamp of the last processed file(backup)',
    ['shifter']
    )
RECENT_BACKUP_SIZE = Gauge(
    'yaook_backup_shifter_recent_backup_size_bytes',
    'Size (in bytes) of the most recent backup processed'
    )
RECENT_BACKUP_TIMESTAMP = Gauge(
    'yaook_backup_shifter_recent_backup_timestamp_seconds',
    'Timestamp of the most recent backup processed'
    )
WARM_BACKUPS_NUM = Gauge(
    'yaook_backup_shifter_warm_backups_total',
    'Number of backups currently in warm storage'
    )
WARM_BACKUPS_SIZE_MAX = Gauge(
    'yaook_backup_shifter_warm_backups_max_size_bytes',
    'Size (in bytes) of the biggest backup currently in warm storage'
    )
WARM_BACKUPS_SIZE_MIN = Gauge(
    'yaook_backup_shifter_warm_backups_min_size_bytes',
    'Size (in bytes) of the smallest backup currently in warm storage'
    )
WARM_BACKUPS_SIZE_TOTAL = Gauge(
    'yaook_backup_shifter_warm_backups_size_total_bytes',
    'Total size (in bytes) of backups currently in warm storage'
    )

_ARCHIVE_PREFIX = "automatically_archived"

_DATETIME_FORMAT = "%Y%m%d_%H:%M:%S.%f"

_PROGRAM_NAME = "backup_shifter"
_NEW_DIRNAME = "new"
_WARM_DIRNAME = "warm"
_SCRATCH_DIRNAME = "scratch"

DEFAULT_COMPRESSION = True

# Default number of days to keep the backups in the _WARM_DIRNAME directory
DEFAULT_NUM_DAYS = -1

# Default max number of backups to keep in the _WARM_DIRNAME directory
DEFAULT_NUM_BACKUPS = 3

# Default number of days between attempts to cleanup after for example a failed
# shifter
DEFAULT_PROCESSING_INTERVAL = 1

# Default logfile
DEFAULT_LOGFILE = None

# Default loglevel (as string)
DEFAULT_LOGLEVEL = "INFO"

_YAOOK_BACKUP_PREFIX = "YAOOK_BACKUP_SHIFTER"


logger = logging.getLogger(_PROGRAM_NAME)


class EnvVarShortName(Enum):
    WORK_DIR = f'{_YAOOK_BACKUP_PREFIX}_WORK_DIR'
    SHIFTERS_DIR = f'{_YAOOK_BACKUP_PREFIX}_SHIFTERS_DIR'
    NUM_DAYS = f'{_YAOOK_BACKUP_PREFIX}_NUM_DAYS'
    NUM_BACKUPS = f'{_YAOOK_BACKUP_PREFIX}_NUM_BACKUPS'
    COMPRESS = f'{_YAOOK_BACKUP_PREFIX}_COMPRESS'
    INTERVAL = f'{_YAOOK_BACKUP_PREFIX}_INTERVAL'
    LOGFILE = f'{_YAOOK_BACKUP_PREFIX}_LOGFILE'
    LOGLEVEL = f'{_YAOOK_BACKUP_PREFIX}_LOGLEVEL'


def _validate_dir_exists(instance, attribute, value):
    if not value.is_dir():
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} must "
            f"point to an existing directory. Was: '{value}'")


def _validate_parent_dir_exists(instance, attribute, value):
    if value is None:
        return
    if not (value.is_file() or not value.exists()):
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} "
            f"must either be non-existent or a file. Was: '{value}'.")
    if not value.parent.is_dir():
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value} must "
            f"point to a (non-existing or existing) file in "
            f"an existing directory. Was: '{value}'.")


def _validate_log_level(instance, attribute, value):
    try:
        logging._checkLevel(value)
    except ValueError:
        raise ValueError(
            f"{getattr(EnvVarShortName, attribute.name.upper()).value}: "
            f"{value} is not a valid loglevel."
        )


@environ.config(prefix=_YAOOK_BACKUP_PREFIX)
class ShifterConfig:
    work_dir = environ.var(
        validator=_validate_dir_exists,
        converter=Path,
        help="The working directory of the backup shifter. "
             "Must be an existing directory.",
    )

    shifters_dir = environ.var(
        validator=_validate_dir_exists,
        converter=Path,
        help="The directory where the shifters are kept. "
             "Must be an existing directory.",
    )

    shifters = environ.var(
        default=None,
        help="The shifters to run from the shifters directory. "
             "If unset defaults to all files in the directory."
    )

    num_days = environ.var(
        converter=int,
        default=DEFAULT_NUM_DAYS,
        help="Max number of days to keep the backups in the "
             f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME} directory. "
             f"A value <= 0 disables deletion of backups due to their age. "
             f"Default value: {DEFAULT_NUM_DAYS}. ",
    )

    num_backups = environ.var(
        converter=int,
        default=DEFAULT_NUM_BACKUPS,
        help="Max number of backups to keep in the "
             f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME} directory. "
             f"A value <= 0 disables deletion of the backups due to their "
             f"quantity. Default value: {DEFAULT_NUM_BACKUPS}. ",
    )

    interval = environ.var(
        converter=int,
        default=DEFAULT_PROCESSING_INTERVAL,
        help="Number of days between attempts to cleanup in "
             f"${EnvVarShortName.WORK_DIR.value}/{_NEW_DIRNAME} in case there "
             f"are some files or directories that could not be processed. "
             f"This could for example happen, if a shifter fails. At the same "
             f"time, files are also removed from "
             f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME} if they are "
             f"older than ${EnvVarShortName.NUM_DAYS.value} days. "
             f"Default value: {DEFAULT_PROCESSING_INTERVAL}. ",
    )

    compress = environ.bool_var(
        default=DEFAULT_COMPRESSION,
        help="Whenever a directory is moved to "
             f"${EnvVarShortName.WORK_DIR.value}/{_NEW_DIRNAME} it is "
             "archived, before being moved to "
             f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME}. This flag "
             f"indicates whether to perform compression when archiving. "
             f"Default value: {DEFAULT_COMPRESSION}. ",
    )

    logfile = environ.var(
        validator=_validate_parent_dir_exists,
        converter=environ_attr.converters.optional(Path),
        default=DEFAULT_LOGFILE,
        help=f"Path to the log file to use. "
             f"Default value: {DEFAULT_LOGFILE}. ",
    )

    loglevel = environ.var(
        validator=_validate_log_level,
        default=DEFAULT_LOGLEVEL,
        help=f"Log level to use. Default value: {DEFAULT_LOGLEVEL}. ",
    )


class BackupShifter(object):
    program_name = _PROGRAM_NAME

    description = ("Backup shifter configurable by the following environment "
                   "variables: "
                   f"{', '.join([e.value for e in EnvVarShortName])}. "
                   f"The directory "
                   f"${EnvVarShortName.WORK_DIR.value}/{_NEW_DIRNAME}, "
                   "will be watched for new entries (files or directories), "
                   "which one at a time will be processed by the shifters in "
                   f"${EnvVarShortName.SHIFTERS_DIR.value} and eventually "
                   f"moved to the directory "
                   f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME}. A "
                   f"total of ${EnvVarShortName.NUM_BACKUPS.value} backups "
                   f"will be kept in "
                   f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME} for "
                   f"${EnvVarShortName.NUM_DAYS.value} days before "
                   f"being deleted. If a directory is present in "
                   f"${EnvVarShortName.WORK_DIR.value}/{_NEW_DIRNAME}, the "
                   "directory will be archived before being backed-up. "
                   "Environment variables: "
                   f"{ShifterConfig.generate_help()}")  # type: ignore

    epilog = ("Only files and directories moved to "
              f"${EnvVarShortName.WORK_DIR.value}/{_NEW_DIRNAME}, "
              "i.e., causing the inotify event IN_MOVED_TO, will be "
              "backed-up. Furthermore, the last backup (in "
              f"${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME}) "
              "of a certain file might be deleted in favor of other backups "
              f"in ${EnvVarShortName.WORK_DIR.value}/{_WARM_DIRNAME}, "
              f"if the limit of ${EnvVarShortName.NUM_BACKUPS.value} "
              "backups is reached. It is therefore recommended to "
              "only use one instance of the backup-shifter per file or "
              "directory, and to make sure to make use of shifters in "
              f"${EnvVarShortName.SHIFTERS_DIR.value} to instantly process "
              "the backups as desired.")

    _shifter_config = None

    def __init__(self):
        if self._shifter_config is None:
            # initialize lazily to allow printing the help message without
            # setting any of the required env vars.
            try:
                self._shifter_config = environ.to_config(ShifterConfig)
            except Exception as e:
                msg = f"Exception: {e}. " \
                      f"Environment variables: {ShifterConfig.generate_help()}"
                raise EnvironmentError(msg)

        self._setup_logger(
            log_level=self._shifter_config.loglevel,
            log_file=self._shifter_config.logfile,
        )

        self.num_days = self._shifter_config.num_days
        self.num_backups = self._shifter_config.num_backups

        self.shifters_dir = self._shifter_config.shifters_dir.resolve()
        self.shifters = self._resolve_shifters(self._shifter_config.shifters)

        self.work_dir = self.new_dir = self.scratch_dir = self.warm_dir = None
        self._setup_work_dir(self._shifter_config.work_dir)

        self._archive_mode = "w"
        self._archive_filename_ext = "tar"
        if self._shifter_config.compress:
            self._archive_mode += ":gz"
            self._archive_filename_ext = "tgz"

        self._pid = os.getpid()
        self._stale_new_files = set()

        self._processing_interval = None
        interval = self._shifter_config.interval
        if interval > 0:
            self._processing_interval = timedelta(days=interval)
        self._last_processing = None

    @staticmethod
    def _setup_logger(log_level, log_file=None):
        formatters = {
            "standard": {
                "format": '%(asctime)s - %(name)s - '
                          '%(levelname)s - %(message)s'
            }
        }
        handlers = {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "standard",
                "level": log_level,
                "stream": "ext://sys.stdout",
            },
        }
        if log_file:
            handlers["file"] = {
                "class": "logging.handlers.RotatingFileHandler",
                "formatter": "standard",
                "filename": log_file,
                "maxBytes": 1024,
                "backupCount": 3,
            }
        loggers = {
            "backup_shifter": {
                "handlers": list(handlers.keys()),
                "level": log_level,
            }
        }
        config = {
            "version": 1,
            "handlers": handlers,
            "formatters": formatters,
            "loggers": loggers,
        }
        logging.config.dictConfig(config)

    def _setup_work_dir(self, work_dir: Path) -> None:
        logger.info("Initializing working directory")

        self.work_dir = work_dir.resolve()

        self.new_dir = self.work_dir / _NEW_DIRNAME
        self.scratch_dir = self.work_dir / _SCRATCH_DIRNAME
        self.warm_dir = self.work_dir / _WARM_DIRNAME

        child_dirs = [self.new_dir, self.scratch_dir, self.warm_dir]
        for d in child_dirs:
            if d.exists():
                if d.is_file():
                    msg = f"{d} must either be a directory or not exist."
                    raise ValueError(msg)
            else:
                d.mkdir()

        for d in child_dirs:
            if os.path.ismount(d.as_posix()):
                msg = f"The directory {d} is a mountpoint. " \
                      f"That is not supported."
                raise ValueError(msg)

        if any(self.scratch_dir.iterdir()):
            raise ValueError(f"{self.scratch_dir} must be empty.")

    def _resolve_shifters(self, shifter_names: typing.Optional[str]
                          ) -> typing.List[Path]:
        shifters = []
        if shifter_names is None:
            shifters = list(self.shifters_dir.iterdir())
            logger.debug("Found these shifters in %s: %s ",
                         self.shifters_dir, shifters)
        else:
            for shifter_name in shifter_names.split(","):
                shifter_name = shifter_name.strip()
                shifters.append(self.shifters_dir / shifter_name)

        for shifter in shifters:
            if not (shifter.is_file()
                    and os.access(shifter.as_posix(), os.X_OK)):
                raise ValueError("Shifter %s must be an executable file.",
                                 shifter)

        return shifters

    def start(self, loop_forever=True):
        """Watches the self.new_dir directory for new files or directories.

        New directories are archived and the resulting archive files are placed
        in self.new_dir.

        To process any new files (including the created archive files), it
        first executes the shifters in self.shifters_dir and then moves the
        files to self.warm_dir.

        When there are more than self.num_backups files or files older than
        self.num_days days in self.warm_dir, the oldest files are removed from
        self.warm_dir.

        self.warm_dir will therefore only briefly contain more than
        self.num_backups files or files older than self.num_days days.

        :param loop_forever: flag indicating whether the outer loop should loop
            forever or only be run once. Used for testing.
        :type loop_forever: bool
        """

        new_notifier = adapters.Inotify()
        new_notifier.add_watch(self.new_dir.as_posix(), mask=IN_MOVED_TO)
        new_notifier.add_watch(self.warm_dir.as_posix(), mask=IN_MOVED_TO)

        while True:
            try:
                logger.info("Initial processing of all files in %s and %s",
                            self.new_dir, self.warm_dir)
                # If there are any files in self.warm_dir, we process them.
                if any(self.warm_dir.iterdir()):
                    self._on_warm()

                # Set _stale_new_files to the current content of
                # self.new_dir...
                self._stale_new_files = set(
                    f.resolve()
                    for f in self.new_dir.iterdir()
                )
                # ...process all those files and directories as new files, and
                # update self._stale_new_files to the set of all files and/or
                # directories for which the processing failed.
                self._stale_processing()
                self._last_processing = time.monotonic()

                logger.info("Receiving inotify events")
                for event in new_notifier.event_gen(yield_nones=True):
                    self._handle_inotify_event(event)
                    self._monitor_backups()
                    if self._is_time_to_process_stale():
                        self._stale_processing()
                        self._last_processing = time.monotonic()

            except (adapters.TerminalEventException,
                    adapters.EventTimeoutException, IOError) as e:
                logger.error("Failed to retrieve events. Retrying. "
                             "(Exception: %s)", e)

            if not loop_forever:
                return

    def _monitor_backups(self) -> None:
        """Collects information about the recent backup
        And all existed warm backups"""
        warm_files = self._sorted_warm_files()
        if warm_files and warm_files != []:
            last_backup = warm_files[-1]
            list_of_sizes = []

            for f in warm_files:
                list_of_sizes.append(f.stat().st_size)

            RECENT_BACKUP_SIZE.set(last_backup.stat().st_size)
            RECENT_BACKUP_TIMESTAMP.set(last_backup.stat().st_mtime)
            WARM_BACKUPS_NUM.set(len(warm_files))
            WARM_BACKUPS_SIZE_MAX.set(max(list_of_sizes))
            WARM_BACKUPS_SIZE_MIN.set(min(list_of_sizes))
            WARM_BACKUPS_SIZE_TOTAL.set(sum(list_of_sizes))
        else:
            RECENT_BACKUP_SIZE.set(0)
            RECENT_BACKUP_TIMESTAMP.set(0)
            WARM_BACKUPS_NUM.set(0)
            WARM_BACKUPS_SIZE_MAX.set(0)
            WARM_BACKUPS_SIZE_MIN.set(0)
            WARM_BACKUPS_SIZE_TOTAL.set(0)

    def _monitor_last_shifter_run(
        self,
        start: float,
        end: float,
        shifter: str,
        filetime: float,
        success: bool
    ) -> None:
        """Collects information about the last run,
        no matter whether successful or not"""

        LAST_RUN_DURATION.labels(shifter=shifter).set(end-start)
        if success:
            LAST_RUN_SUCCESS.labels(shifter=shifter).set(1)
        else:
            LAST_RUN_SUCCESS.labels(shifter=shifter).set(0)
        LAST_RUN_TIMESTAMP.labels(shifter=shifter).set(end)
        LAST_RUN_FILE_TIMESTAMP.labels(shifter=shifter).set(filetime)

    def _is_time_to_process_stale(self) -> bool:
        """
        :return: True if processing is enabled (i.e. if
            self._processing_interval > 0) and it was more than
            self._processing_interval days since self._last_processing
            timestamp, or if self._last_processing is None. False otherwise.
        :rtype bool
        """
        if not self._processing_interval:
            return False
        if self._last_processing is None:
            return True
        next_time = \
            self._last_processing + self._processing_interval.total_seconds()
        return time.monotonic() >= next_time

    def _handle_inotify_event(self, event: typing.Optional[tuple]) -> None:
        noticed_path = self._extract_and_validate_inotify_event(event)
        if noticed_path:
            self._handle_noticed_path(noticed_path)

    def _handle_noticed_path(self, noticed_path: Path) -> None:
        if self.new_dir.samefile(noticed_path.parent):
            logger.info("Noticed new file/dir: %s", noticed_path)
            success = self._on_new(noticed_path)
            if not success:
                logger.error("Failed to process %s", noticed_path)
                self._stale_new_files.add(noticed_path.resolve())
        elif self.warm_dir.samefile(noticed_path.parent):
            logger.info("Noticed warm file %s", noticed_path)
            self._on_warm()
        else:
            logger.error("Inconsistent state. Noticed a file/dir (%s), which "
                         "is neither in %s or %s, although it has already "
                         "been validated. Continuing.",
                         noticed_path, self.new_dir, self.warm_dir)

    def _stale_processing(self):
        """Remove backups in self.warm_dir older than self.num_days and
        initiate backups of stale files in self.new_dir.

        On success a file is moved to self.warm_dir after having been processed
        by the shifters in self.shifters_dir.
        On success a directory is archived and placed as a file
        in self.new_dir.

        On failure a file or directory is left in self.new_dir.
        """
        logger.info("Periodic processing of stale files in %s", self.new_dir)
        self._stale_new_files = \
            self._process_new_files(files=self._stale_new_files)
        logger.info("Periodic processing of stale files in %s", self.warm_dir)
        self._remove_old_warm_files()

    def _extract_and_validate_inotify_event(
            self, event: typing.Optional[tuple]) -> typing.Optional[Path]:
        """Given an event as returned by inotify.event_gen(),
        extract the file path that caused the event and return it.

        :param event: A tuple of inotify event information:
            (event, event_types, path, filename). Note: event is ignored
        :type event: typing.Optional[
                tuple[(adapters._INOTIFY_EVENT, list[str], str, str]
            ]

        :return: The path of the file that caused the event, or None on
            failure, or missing event
        :rtype: typing.Optional[Path]
        """
        if not event:
            return None
        try:
            (_, type_names, path, filename) = event
            logger.debug("PATH=[%s] FILENAME=[%s] EVENT_TYPES=%s",
                         path, filename, type_names)
            noticed_path = Path(path, filename).resolve()
        except (KeyError, RuntimeError, ValueError, OSError) as e:
            logger.error("Could not retrieve information from the received "
                         "event '%s'. Skipping. Exception: %s", event, e)
            return None

        if set(type_names) == {MASK_LOOKUP[IN_MOVED_TO],
                               MASK_LOOKUP[IN_ISDIR]}:
            was_dir = True
        elif set(type_names) == {MASK_LOOKUP[IN_MOVED_TO]}:
            was_dir = False
        else:
            logger.error("Unexepected inotify mask(s) '%s' for file or "
                         "directory '%s'. Skipping event %s.",
                         type_names, noticed_path, event)
            return None

        if noticed_path.parent != self.warm_dir and \
                noticed_path.parent != self.new_dir:
            logger.error("Could not handle event %s. Inconsitent state. A "
                         "file/directory (%s) was noticed in an unexpected "
                         "directory: %s.", event, noticed_path,
                         noticed_path.parent)
            return None

        if noticed_path.is_dir() != was_dir and noticed_path.exists():
            msg_fmt = "Could not handle event %s. Inconsitent state. "
            if was_dir:
                msg_fmt += "The inotify event IN_ISDIR was sent but the " \
                           "file in question (%s) is not a directory."
            else:
                msg_fmt += "The inotify event IN_ISDIR was not sent but the " \
                           "file in question (%s) is a directory."
            logger.error(msg_fmt, event, noticed_path)
            return None

        if noticed_path.parent == self.warm_dir and was_dir:
            logger.error("Could not handle event %s. Inconsitent state. A "
                         "directory (%s) was added to %s.", event,
                         noticed_path, self.warm_dir)
            return None

        return noticed_path

    def _remove_old_warm_files(self):
        """Remove all backups in self.warm_dir older than self.num_days days,
        unless self.num_days <= 0."""
        logger.debug("Checking whether there are any files that are too old "
                     "in the warm directory.")
        if self.num_days <= 0:
            return
        for warm_file in self.warm_dir.iterdir():
            try:
                if self._is_too_old(warm_file):
                    logger.info("Deleting warm file %s older than %d day(s).",
                                warm_file, self.num_days)
                    warm_file.unlink()
            except FileNotFoundError as e:
                logger.debug("%s: Warm file %s had already been deleted. "
                             "Continuing", warm_file, e.filename)
                continue

            except IsADirectoryError as e:
                logger.error("%s: Inconsistent state. Unexpectedly found a "
                             "directory in %s: %s. Skipping.",
                             warm_file, self.warm_dir, e.filename)
                continue

    def _is_too_old(self, warm_file: Path) -> bool:
        """Check whether warm_file is older than allowed (i.e. older than
        self.num_days days)

        :param warm_file: The file to check
        :type warm_file: Path
        :return: True if warm_file was created longer than self.num_days days
            ago. False otherwise.
        :rtype: bool
        """
        if self.num_days <= 0:
            return False
        backup_date = datetime.utcfromtimestamp(warm_file.stat().st_mtime)
        return backup_date + timedelta(days=self.num_days) < datetime.utcnow()

    @staticmethod
    def _now_as_str() -> str:
        """Returns the current time in utc time zone as string """
        return datetime.now(timezone.utc).strftime(_DATETIME_FORMAT)

    def _on_warm(self):
        """Remove the oldest backup if there are too many."""
        logger.debug("Checking whether to remove old files from warm "
                     "directory %s", self.warm_dir)
        # we only act if only a limited number of backups are allowed
        if self.num_backups <= 0:
            return

        warm_files = self._sorted_warm_files()
        backups_2b_deleted = warm_files[self.num_backups:]
        if not backups_2b_deleted:
            logger.debug("Not deleting any files from %s, since there were "
                         "only %d present and %d are allowed.",
                         self.warm_dir, len(warm_files), self.num_backups)
            return

        logger.info("Deleting %d files from %s, since there were %d (> %d) "
                    "backups present.", len(backups_2b_deleted),
                    self.warm_dir, len(warm_files), self.num_backups)
        for old_backup in backups_2b_deleted:
            try:
                logger.info("        Deleting %s", old_backup)
                old_backup.unlink()
            except FileNotFoundError as e:
                logger.debug("Warm file %s had already been deleted. "
                             "Continuing", e.filename)
                continue

            except IsADirectoryError as e:
                logger.error("Inconsistent state. Unexpectedly found a "
                             "directory in %s: %s. Skipping.",
                             self.warm_dir, e.filename)
                continue

    def _sorted_warm_files(self) -> list:
        """sort all files (excluding the directories) in self.warm_dir
        according to the mtime and return them as a list with the oldest last.

        :rtype list(Path)
        """
        files = [f for f in self.warm_dir.iterdir() if f.is_file()]
        return sorted(
            files,
            key=lambda f: f.stat().st_mtime_ns,
            reverse=True
        )

    def _process_new_files(self, files: set) -> set:
        """Process the files, which are assumed to be located in self.new_dir

        On success a file is moved to self.warm_dir after having been processed
        by the shifters in self.shifters_dir.
        On success a directory is archived and placed as a file in
        self.new_dir.

        On failure a file or directory is left in self.new_dir.

        :param files: the files to process
        :type files: set[Path]
        :return: all files and directories that could not be processed.
        :rtype set[Path]
        """
        unprocessed_files = set()
        for file_or_dir in files:
            success = self._on_new(file_or_dir)
            if not success:
                logger.error("The 'new' file or directory '%s' could not be "
                             "processed.", file_or_dir)
                unprocessed_files.add(file_or_dir.resolve())
        return unprocessed_files

    def _on_new(self, file_or_dir: Path) -> bool:
        """Process the file or directory pointed to by file_or_dir.

        If it is a file, it will be backed up and moved to self.warm_dir.

        If it is a directory, it will be archived and its archive file
        moved to self.new_dir.

        Note: file_or_dir is assumed to be located in self.new_dir.

        :param file_or_dir: path pointing to the file or drectory which should
            be backed-up
        :type file_or_dir: Path

        :return: True if the backup or archiving was successful, False othewise
        :rtype bool
        """
        if file_or_dir.is_dir():
            try:
                # If the directory entry is a directory we archive it and put
                # it as s file into new_dir. This way the watch is triggered
                # again and _on_new() will eventually be called again.
                logger.info("%s: is a directory, processing", file_or_dir)
                self._archive_directory(dirname=file_or_dir.name)
                logger.debug("Successfully archived directory %s", file_or_dir)
                return True
            except Exception as e:
                logger.error("Failed to archive directory %s. Exception: %s",
                             file_or_dir, e)
                return False
        if file_or_dir.is_file():
            logger.info("%s: is a file, processing", file_or_dir)
            return self._backup_file(file_or_dir)
        logger.error("Failed to process '%s'. It is neither a file nor a "
                     "directory.", file_or_dir)
        self._monitor_backups()
        return False

    def _get_archive_filename(self, dirname: str) -> str:
        return f"{_ARCHIVE_PREFIX}-{self._pid}-{self._now_as_str()}-" \
               f"{dirname}.{self._archive_filename_ext}"

    @classmethod
    def _get_warm_filename(cls, filename: str) -> str:
        return f"{cls._now_as_str()}-{filename}"

    def _archive_directory(self, dirname: str) -> None:
        """Replace the directory dirname in self.new_dir by an archive file"""
        fname = self._get_archive_filename(dirname)
        scratch_file = self.scratch_dir / fname

        try:
            full_source_path = self.new_dir / dirname
            with tarfile.open(scratch_file, self._archive_mode) as tar:
                tar.add(full_source_path, arcname=dirname)

            scratch_file.rename(self.new_dir / fname)
            shutil.rmtree(full_source_path)
        finally:
            try:
                scratch_file.unlink()
            except OSError:
                pass

    def _backup_file(self, source_file: Path) -> bool:
        """Run the shifters in self.shifters_dir on source_file and move the
        file to self.warm_dir if they were all successful.

        :param source_file: File to backup
        :type source_file: Path

        :return True if source_file was moved to self.warm_dir, False otherwise
        :rtype bool
        """
        success = self._run_shifters(source_file)
        if success:
            fname = self._get_warm_filename(source_file.name)
            target_file = self.warm_dir / fname
            source_file.rename(target_file)
        return success

    def _run_shifters(self, source_file: Path) -> bool:
        """Runs all the executables in the self.shifters_dir directory with the
        source_file path as an argument.

        :param source_file: The path to the file which should be given as
            argument to the shifters.
        :type source_file: Path
        :return: True if all shifters in self.shifters_dir were successful,
            False otherwise.
        :rtype bool
        """
        success = True
        successful_shifters = []
        failing_shifters = []
        all_shifters = list(self.shifters)
        for shifter in all_shifters:
            logger.info("%s: shifter %s processing", source_file, shifter)
            start_timestamp = time.monotonic()
            try:
                self._run_shifter(shifter, source_file)
                logger.info("%s: shifter %s processing complete",
                            source_file, shifter)
                successful_shifters.append(shifter)
            except (subprocess.CalledProcessError, OSError) as e:
                success = False
                logger.error("Shifter %s failed. Exception: %s", shifter, e)
                failing_shifters.append(shifter)
                continue
            except Exception as e:
                success = False
                logger.error("Shifter %s failed. Exception of type '%s': %s",
                             shifter, type(e), e)
                failing_shifters.append(shifter)
                continue
            finally:
                end_timestamp = time.monotonic()
                self._monitor_last_shifter_run(start_timestamp, end_timestamp,
                                               os.path.basename(shifter),
                                               os.path.getmtime(source_file),
                                               success)

        logger.info("%s: %d (of %d) shifters succeeded.",
                    source_file, len(successful_shifters), len(all_shifters))
        logger.debug("Successful shifters: %s", successful_shifters)
        if failing_shifters:
            logger.warning("%s: %d shifters failed: %s",
                           source_file, len(failing_shifters),
                           failing_shifters)
        return success

    @staticmethod
    def _run_shifter(shifter: Path, backup: Path) -> None:
        """Runs the executable given by the shifter path with the backup path
        as an argument.

        Raises:
            subprocess.CalledProcessError: if the shifter returned a non-zero
                return code.
        """
        shifter_str = shifter.as_posix()
        backup_str = backup.as_posix()
        logger.debug("Running shifter %s on %s", shifter_str, backup_str)
        completed_process = subprocess.run([shifter_str, backup_str])
        logger.debug("Shifter %s completed on %s. Checking return code.",
                     shifter_str, backup_str)
        completed_process.check_returncode()
