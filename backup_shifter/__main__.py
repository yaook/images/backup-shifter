#!/usr/bin/env python
#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import argparse
from .backup_shifter import BackupShifter
from prometheus_client import start_http_server
import os

# Default port to provide metrics
DEFAULT_METRICS_PORT = 9100

parser = argparse.ArgumentParser(
    prog=BackupShifter.program_name,
    description=BackupShifter.description,
    epilog=BackupShifter.epilog,
)


if __name__ == "__main__":
    args = parser.parse_args()
    # Start up the prometheus server.
    metrics_port = int(os.environ.get(
        "YAOOK_BACKUP_SHIFTER_METRICS_PORT",
        DEFAULT_METRICS_PORT
    ))
    start_http_server(metrics_port)
    BackupShifter().start()
