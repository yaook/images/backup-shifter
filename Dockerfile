##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.13.1-bookworm

RUN apt update && \
    apt install tini -y

ADD backup_shifter /app/backup_shifter
ADD shifters /app/shifters
ADD requirements.txt /app/

RUN pip3 install -r /app/requirements.txt

WORKDIR /app

ENV YAOOK_BACKUP_SHIFTER_SHIFTERS_DIR=/app/shifters

ENTRYPOINT /usr/bin/tini -s -- python3 -m backup_shifter
