<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# Backup Shifter

## Requirements

- Transfer locally created backups out of the system (off-site)
- Do not transfer partial backups
- Do not block new backups
- Allow deletion of old backups

## Design

Broader design:

- Three directories: `incoming`, `scratchpad`, `new`, `warm`. All must be on
  the same filesystem.
- `incoming` is irrelevant to shifter.
- shifter watches `new` for `rename` inotify events.
- files/directories `rename`-d into `new` will be *shifted*. files/directories
  which have been shifted completely are `rename`-d into `warm`.
- at most N backups are kept in `warm`, older ones are deleted.

The location of a backup serves as marker about whether it has been processed.
This is nice because renames are atomic on POSIX. Worst case is that we will
re-process an old backup.

## Implementation

### Watch on `new`

Watch for `rename` events. For each rename:

1. if directory:
    1. convert to tarfile with optional compression in `scratchpad`
    2. move backup from `scratchpad` to `new` (this triggers a new inotify
       event for the compressed file)
    3. delete source from `scratchpad`
    4. exit
2. if file:
    1. run all shifters
    2. if all shifters were successful, move backup file to `warm` (this
       triggers a new inotify in `warm`)

### Watch on `warm`

Watch for `rename` event. For each rename:

1. check all files against retention policy
2. delete files not to be retained

This can also be executed periodically.
